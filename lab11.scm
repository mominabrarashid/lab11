(define (aCompletelyUselessFunctionCreatedOnlyToMakeYourLifeMiserable a b)
		(IFELSE (OR (LESS a b) (ISZERO a))
		(iter b sum 0)
		(ADD a b)))
		
(define sum 0)
(define (iter n sum i)
	    (if (zero? n)
			sum
	        (iter (- n 1) (+ sum i) (+ i 1))))
			
(define ADD(lambda (a b)         
		(s a (s b 0))))
		 
(define SUBTRACT(lambda (a b)         
		(p b a)))
		
(define AND(lambda (M N)         
		 (N (M TRUE FALSE) FALSE)))
		 
(define OR(lambda (M N)         
		 (N TRUE (M TRUE FALSE))))

(define NOT(lambda (M)         
		 (M FALSE TRUE)))
		 
(define TRUE(lambda (a b)         
		 a ))
		 
(define FALSE(lambda (a b)         
		 b ))

(define LESS(lambda (a b)       
		(if (< a b) 
		TRUE
		FALSE)))
		 
(define (s a b)
	  (+ a b))
	  
(define (p a b)
	  (if (> a b)
       0
      (- b a)))
	  
(define (ISZERO a)
	  (if (zero? a)
		TRUE
		FALSE
	  ))

(define (IFELSE a b c)
	  ( a b c) )